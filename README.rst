========
Overview
========

This is meant to be a collection of potentially useful routines for use in 
the Meltzer Lab to perform NGS data analysis tasks.  It also contains some
documentation of commonly-used tools.  `Read the Docs for details 
<https://seqtools.readthedocs.org/>`_.
